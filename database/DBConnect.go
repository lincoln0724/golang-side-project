package database

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
)

var DBconnent *gorm.DB

var err error

func DD() {
	// refer https://github.com/go-sql-driver/mysql#dsn-data-source-name for details
	dsn := "DevAuth:Dev127336@tcp(127.0.0.1:3306)/MyWorld?charset=utf8mb4&parseTime=True&loc=Local"
	DBconnent, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
}
