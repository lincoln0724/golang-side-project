package model

import (
	"log"
	"sideProject/database"
)

type User struct { // DB : Users
	Id       int    `json:"UserID" binding:"required"`                           //Id DB: id, UserId DB: user_id
	Name     string `json:"UserName" gorm:"Column:name" binding:"required,gt=3"` //定義column name
	Password string `json:"UserPassword" binding:"min=4,max=20,userpassword"`
	Email    string `json:"UserEmail" binding:"email"`
}

func (User) TableName() string { //重新定義table name ，不存在的話，會照+s規則->users
	return "users"
}

func CreateUser(user User) User {
	database.DBconnent.Create(&user)
	return user
}

func DeleteUser(userId string) bool {
	var user User
	if err := database.DBconnent.Where("id = ?", userId).Delete(&user).Error; err != nil {
		log.Println("delete user failed error : ", err.Error())
		return false
	}
	return true
}

func UpdateUser(userId string, user User) User {
	database.DBconnent.Model(&user).Where("id = ?", userId).Updates(user)
	return user
}

func CheckUserPassword(name string, password string) User {
	user := User{}
	database.DBconnent.Where("name = ? and password = ?", name, password).First(&user)
	return user
}
