package routes

import (
	"github.com/gin-gonic/gin"
	session "sideProject/middlewares"
	"sideProject/model"
	"sideProject/service"
)

func AddUserRouter(r *gin.RouterGroup) {
	user := r.Group("/users", session.SetSession())

	user.GET("/", service.CacheAllDecorator(service.RedisAllUser, "user_all", model.User{}))
	user.GET("/:id", service.CacheDecorator(service.RedisUser, "id", "user_%s", model.User{}))
	user.POST("/login", service.LoginUser)
	user.GET("/check", service.CheckUserSession)

	user.Use(session.AuthSession())
	{
		user.POST("/", service.PostUser)
		user.PUT("/:id", service.PutUser)
		user.DELETE("/:id", service.DeleteUser)
		user.GET("/logout", service.LogoutUser)
	}
}
