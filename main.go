package main

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"io"
	"log"
	"os"
	DB "sideProject/database"
	"sideProject/middlewares"
	"sideProject/routes"
)

func setupLogging() {
	f, _ := os.Create("gin.log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

func main() {
	setupLogging()
	router := gin.Default()

	//註冊Validator
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		err := v.RegisterValidation("userpassword", middlewares.UserPassword)
		if err != nil {
			log.Println("RegisterValidation error : " + err.Error())
			return
		}
	}
	//router.Use(gin.BasicAuth(gin.Accounts{"Lincoln": "lincoln1234"})) //基本的身份驗證，全routes
	//router.Use(gin.Recovery())	//發生錯誤返回500，會包含在gin.Default中
	router.Use(middlewares.Logger())
	v1 := router.Group("/v1")
	routes.AddUserRouter(v1)

	go func() {
		DB.DD()
	}()

	err := router.Run(":8000")
	if err != nil {
		return
	}
}
