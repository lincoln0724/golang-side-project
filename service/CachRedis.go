package service

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gomodule/redigo/redis"
	"github.com/pquerna/ffjson/ffjson"
	"log"
	"net/http"
	red "sideProject/database"
)

func CacheDecorator(h gin.HandlerFunc, param string, readisKeyPattern string, modelInterface interface{}) gin.HandlerFunc {
	return func(c *gin.Context) {
		KeyId := c.Param(param)
		redisKey := fmt.Sprintf(readisKeyPattern, KeyId)
		conn := red.RedisDefaultPool.Get()
		defer func(conn redis.Conn) {
			err := conn.Close()
			if err != nil {
				log.Println("CacheDecorator redis close error : ", err.Error())
				return
			}
		}(conn)
		data, err := redis.Bytes(conn.Do("GET", redisKey))
		if err != nil {
			h(c)
			dbResult, exists := c.Get("dbUser")
			if !exists {
				dbResult = modelInterface
			}
			redisData, _ := ffjson.Marshal(dbResult)
			_, err := conn.Do("SETEX", redisKey, 30, redisData)
			if err != nil {
				log.Println("CacheDecorator redis Do SETEX error : ", err.Error())
				return
			}
			c.JSON(http.StatusOK, gin.H{
				"message": "Form DB",
				"data":    dbResult,
			})
			return
		}
		err = ffjson.Unmarshal(data, &modelInterface)
		if err != nil {
			log.Println("CacheDecorator ffjson Unmarshal error : ", err.Error())
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"message": "From Redis",
			"data":    modelInterface,
		})
	}
}

func CacheAllDecorator(h gin.HandlerFunc, readisKeyPattern string, modelInterface interface{}) gin.HandlerFunc {
	return func(c *gin.Context) {
		conn := red.RedisDefaultPool.Get()
		defer func(conn redis.Conn) {
			err := conn.Close()
			if err != nil {
				log.Println("CacheAllDecorator redis close failed error : " + err.Error())
			}
		}(conn)

		data, err := redis.Bytes(conn.Do("GET", readisKeyPattern))
		if err != nil {
			h(c)
			dbUserAll, exists := c.Get("dbUserAll")
			if !exists {
				dbUserAll = modelInterface
			}
			redisData, _ := ffjson.Marshal(dbUserAll)
			_, err := conn.Do("SETEX", readisKeyPattern, 30, redisData)
			if err != nil {
				log.Println("CacheAllDecorator redis Do SETEX error : ", err.Error())
				return
			}
			c.JSON(http.StatusOK, gin.H{
				"message": "From DB",
				"data":    dbUserAll,
			})
			return
		}
		err = ffjson.Unmarshal(data, &modelInterface)
		if err != nil {
			log.Println("CacheAllDecorator ffjson Unmarshal error : ", err.Error())
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"message": "From Redis",
			"data":    modelInterface,
		})
	}
}
