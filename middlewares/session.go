package middlewares

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"net/http"
)

const userkey = "session_id"

func SetSession() gin.HandlerFunc {
	store := cookie.NewStore([]byte(userkey))
	return sessions.Sessions("mysession", store)
}

func AuthSession() gin.HandlerFunc {
	return func(c *gin.Context) {
		session := sessions.Default(c)
		sessionId := session.Get(userkey)
		if sessionId == nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"message": "Please login!",
			})
			c.Abort()
		}
		c.Next()
	}
}

func SaveSession(c *gin.Context, UserId int) {
	session := sessions.Default(c)
	session.Set(userkey, UserId)
	session.Save()
}

func ClearSession(c *gin.Context) {
	session := sessions.Default(c)
	session.Clear()
	session.Save()
}

func GetSession(c *gin.Context) int {
	session := sessions.Default(c)
	sessionId := session.Get(userkey)
	if sessionId == nil {
		return 0
	}
	return sessionId.(int)
}
